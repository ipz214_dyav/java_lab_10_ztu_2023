package com.education.ztu.main;

import com.education.ztu.game.Participant;
import com.education.ztu.game.Schoolar;
import com.education.ztu.game.Team;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Task6 {
    public static void main(String[] args) throws JsonProcessingException {
        Schoolar schoolar1 = new Schoolar("Ivan", 13);
        Schoolar schoolar2 = new Schoolar("Ivan", 13);
        Schoolar schoolar3 = new Schoolar("Ivan", 13);

        Team<Schoolar> team = new Team<>("Dragon");
        team.addNewParticipant(schoolar1);
        team.addNewParticipant(schoolar2);
        team.addNewParticipant(schoolar3);


        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);


        String json = mapper.writeValueAsString(team);

        System.out.println(json);

        Team<Schoolar> team2 = mapper.readValue(json,new TypeReference<Team<Schoolar>>() {});

        System.out.println(team2.getName());
        for (Participant participant : team2.getParticipants()) {
            System.out.println(participant.toString());
        }
    }
}
