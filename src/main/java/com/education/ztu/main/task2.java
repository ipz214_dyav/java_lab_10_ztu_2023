package com.education.ztu.main;

import com.education.ztu.game.Schoolar;
import com.education.ztu.game.Team;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class task2 {
    public static void main(String[] args) throws IOException {
        Schoolar schoolar1 = new Schoolar("Ivan", 13);
        Schoolar schoolar2 = new Schoolar("Mari", 15);
        Schoolar schoolar3 = new Schoolar("dari", 51);

        Team<Schoolar> scollarTeam = new Team<>("Dragon");

        scollarTeam.addNewParticipant(schoolar1);
        scollarTeam.addNewParticipant(schoolar2);
        scollarTeam.addNewParticipant(schoolar3);



        FileOutputStream outputStream = new FileOutputStream("D:\\Java\\Labs\\Java_labs_ztu\\SerFiles\\saveScholarTeam.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

        objectOutputStream.writeObject(scollarTeam);

        objectOutputStream.close();
    }
}
