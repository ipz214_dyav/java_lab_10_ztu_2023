package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;

public class Schoolar extends Participant implements Serializable {

    /** Унікальний ідентифікатор версії серіалізації */
    @Serial
    private static final long serialVersionUID = 8492403516154537518L;

    /**
     * Конструктор класу Schoolar, який викликає конструктор батьківського класу Participant.
     *
     * @param name ім'я учасника
     * @param age вік учасника
     */
    public Schoolar(String name, int age) {
        super(name, age);
    }

    /**
     * Конструктор без параметрів
     * потрібен для серіалізації за допомогою Jackson*/
    public Schoolar(){
        super();
    }

    /**
     * Перевизначений метод toString() для представлення об'єкта Schoolar у вигляді рядка.
     *
     * @return рядок із представленням об'єкта Schoolar
     */
    @Override
    public String toString() {
        return "Schoolar{" +
                "name=" + super.getName() +
                ", age='" + super.getAge() + '\'' + '}';
    }
}
