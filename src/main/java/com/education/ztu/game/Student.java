package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;

/**
 * Клас Student є підкласом класу Participant та реалізує інтерфейс Serializable.
 */
public class Student extends Participant implements Serializable {

    /** Унікальний ідентифікатор версії серіалізації */
    @Serial
    private static final long serialVersionUID = 3671132785125387142L;

    /**
     * Конструктор класу Student, який викликає конструктор батьківського класу Participant.
     *
     * @param name ім'я учасника
     * @param age вік учасника
     */
    public Student(String name, int age) {
        super(name, age);
    }

    /**
     * Перевизначений метод toString() для представлення об'єкта Student у вигляді рядка.
     *
     * @return рядок із представленням об'єкта Student
     */
    @Override
    public String toString() {
        return "Student{" +
                "name=" + super.getName() +
                ", age='" + super.getAge() + '\'' + '}';
    }
}
