package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;

public class Employee extends Participant implements Serializable {

    @Serial
    private static final long serialVersionUID = -4841501021028057049L;

    /**
     * Конструктор класу Employee, який викликає конструктор батьківського класу Participant.
     *
     * @param name 
     * @param age 
     */
    public Employee(String name, int age) {
        super(name, age);
    }

    /**
     * Перевизначений метод toString() для представлення об'єкта класу Employee у вигляді рядка.
     *
     * @return рядок з інформацією про працівника (ім'я та вік)
     */
    @Override
    public String toString() {
        return "Employee{" +
                "name=" + super.getName() + // Отримання ім'я учасника за допомогою методу getName() у батьківському класі
                ", age='" + super.getAge() + '\'' + // Отримання віку учасника за допомогою методу getAge() у батьківському класі
                '}';
    }
}
