package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Клас Team реалізує клонування та серіалізацію.
 *
 * @param <P> тип учасників команди
 */
public class Team<P> implements Cloneable, Serializable {
    @Serial
    private static final long serialVersionUID = -6232387549248332229L;
    /**Назва команди*/
    private String name;
    /**Список учасників команди */
    private List<P> participants = new ArrayList<>();

    /**
     * Конструктор класу Team для встановлення назви команди.
     *
     * @param name назва команди
     */
    public Team(String name) {
        this.name = name;
    }

    /**
     * конструктор без параметрів
     * потрібен для серіалізації та десиріалізації
     * з використанням бібліотеки Jackson*/
    public Team() {}

    /**
     * Метод для додавання нового учасника до команди.
     *
     * @param participant об'єкт учасника, який додається до команди
     */
    public void addNewParticipant(P participant) {
        participants.add(participant);
        if (participant instanceof Participant) {
            System.out.println("To the team " + name + " was added participant " + ((Participant) participant).getName());
        } else {
            System.out.println("To the team " + name + " was added participant " + participant);
        }
    }

    /**
     * Метод для проведення змагання між двома командами.
     *
     * @param team команда, з якою проводиться змагання
     */
    public void playWith(Team<P> team) {
        String winnerName;
        Random random = new Random();
        int i = random.nextInt(2);
        if (i == 0) {
            winnerName = this.name;
        } else {
            winnerName = team.name;
        }
        System.out.println("The team " + winnerName + " is winner!");
    }

    /**
     * Геттер для отримання назви команди.
     *
     * @return назва команди
     */
    public String getName() {
        return name;
    }

    /**
     * Геттер для отримання списку учасників команди.
     *
     * @return список учасників команди
     */
    public List<P> getParticipants() {
        return participants;
    }

    /**
     * Сеттер для встановлення назви команди.
     *
     * @param name назва команди
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Сеттер для встановлення списку учасників команди.
     *
     * @param participants список учасників команди
     */
    public void setParticipants(List<P> participants) {
        this.participants = participants;
    }

    /**
     * Перевизначений метод клонування команди.
     *
     * @return клонована команда
     */
    @Override
    @SuppressWarnings("unchecked")
    public Team<P> clone() {
        try {
            Team<P> clonedTeam = (Team<P>) super.clone();

            List<P> clonedParticipants = new ArrayList<>();
            for (P participant : participants) {
                if (participant instanceof Cloneable) {
                    clonedParticipants.add((P) ((Participant) participant).clone());
                } else {
                    clonedParticipants.add(participant);
                }
            }

            clonedTeam.setParticipants(clonedParticipants);

            return clonedTeam;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
