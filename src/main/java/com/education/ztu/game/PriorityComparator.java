package com.education.ztu.game;

import java.io.Serial;
import java.io.Serializable;
import java.util.Comparator;


public class PriorityComparator implements Serializable {

    /** Унікальний ідентифікатор версії серіалізації */
    @Serial
    private static final long serialVersionUID = 4729030183131241026L;


    public PriorityComparator() {
    }

    /**
     * Статичний метод, який повертає компаратор для порівняння учасників за ім'ям та віком.
     *
     * @return Компаратор для порівняння учасників
     */
    public static Comparator<Participant> getNameAgeComparator() {
        return Comparator.comparing(Participant::getName) 
                .thenComparingInt(Participant::getAge); 
    }
}
